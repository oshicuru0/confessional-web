import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
export interface Confession {
  confession: string;
  id: string;
  numOfViews: string;
  ownerId: string;
  pending: boolean;
  timestamp: number;
  confession_info: object;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {
  public confessions: Observable<any[]>;

  constructor(db: AngularFireDatabase) {
      this.confessions = db.list('/confessionals', ref => ref.orderByChild('pending').equalTo(false)).valueChanges();
  }

  ngOnInit() {
  }

}
