import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ConfessDialogComponent} from '../confess-dialog/confess-dialog.component';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})

export class HeaderComponent implements OnInit {
  animal: string;
  name: string;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfessDialogComponent, {
      width: '600px',
      height: '400px',
      data: {name: 'qweqweqweqweqw', animal: 'efssfwe'}
    });

    dialogRef.afterClosed().subscribe(result => {});
  }
}
