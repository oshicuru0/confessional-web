import {Component, Input, OnInit} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {Observable} from 'rxjs';

export interface Confession {
  confession: string;
  id: string;
  numOfViews: number;
  ownerId: string;
  pending: boolean;
  timestamp: number;
  confession_info: object;
}

@Component({
  selector: 'app-confess-item',
  templateUrl: './confess-item.component.html',
  styleUrls: ['./confess-item.component.css']
})

export class ConfessItemComponent implements OnInit {
  @Input()
  public confession: Confession;
  public comments: Observable<any[]>;
  public db: AngularFireDatabase;
  public shouldShowComments = false;
  public numOfUpVotes = 0;
  public numOfDownVotes = 0;
  public numOfComments = 0;

  constructor(db: AngularFireDatabase) {
    this.db = db;
  }

  ngOnInit() {
    this.findVotes();
    this.comments = this.db.list('/comments/' + this.confession.id).valueChanges();
    this.comments.subscribe(result => {
      this.numOfComments = result.length;
    });
  }

  toggleComments() {
    this.shouldShowComments = !this.shouldShowComments;
  }

  findVotes() {
    for (const key in this.confession.confession_info) {
      if (this.confession.confession_info[key]) {
        this.numOfUpVotes++;
      } else {
        this.numOfDownVotes++;
      }
    }
  }

  transformDate() {
    const currentTime = new Date().getTime();
    const resultTime = currentTime - this.confession.timestamp;
    const hours = ((resultTime / (1000 * 60 * 60)) % 24);
    const minutes = ((resultTime / (1000 * 60)));
    const seconds = ((resultTime / (1000)));
    const days = ((resultTime / (1000 * 60 * 60 * 24)));

    if (days > 0) {
      return Math.round(days) + 'd ago';
    } else if (hours > 0) {
      return Math.round(hours) + 'h ago';
    } else if (minutes > 0) {
      return Math.round(minutes) + 'm ago';
    } else if (seconds > 0) {
      return Math.round(seconds) + 's ago';
    }

    return Math.round(seconds) + 's ago';
  }
}
