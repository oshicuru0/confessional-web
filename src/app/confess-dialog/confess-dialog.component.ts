import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DialogData} from '../header/header.component';
import {AngularFireDatabase} from 'angularfire2/database';

@Component({
  selector: 'app-confess-dialog',
  templateUrl: './confess-dialog.component.html',
  styleUrls: ['./confess-dialog.component.css']
})
export class ConfessDialogComponent implements OnInit {
  public confessContent = '';

  constructor(
    public dialogRef: MatDialogRef<ConfessDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, public db: AngularFireDatabase) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onConfessClick(): void {
    const key = this.db.createPushId();
    this.db.database.ref('/confessionals').child(key).set({
      id: key,
      confession: this.confessContent,
      timestamp: new Date().getTime(),
      numOfViews: 0,
      pending: false
    });
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
